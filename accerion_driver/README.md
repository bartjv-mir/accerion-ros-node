Jupiter_Node

With this ROS node all relevant data from Accerion's positioning sensor Jupiter gets published into ROS. 


--How to install?
clone the folder 'jupiter_node' into your catkin_ws/src folder 
and run in the top folder the catkin_make command 

--How to run? 
In the launch folder of the repository are two launch-files.
The Jupi.launch file starts a single Jupiter node with the parameters readed from params/jupiter_single.yaml
It is suggested to create a new parameter file outside the repository, to avoid overwriting it with repository updates.

If you have multiple Jupiters there is an example for two Jupiters one at the front, one in the back.
Similar to this launch file you can expand your amount from Jupiters like you want.
For every additional Jupiter you should create an additional parameter file in the specific namespace of the node.

--How to configure for your sensor?
In the folder params is the file where all necessary settings can be done. 
The most important thing is to set the Serial NR from your sensor to the parameters.

Quick steps for single Jupiter:
1. 	copy jupiter_node folder to your <catkin_ws>/src
2. 	build your workspace (e.g. catkin_make)
3.	edit the parameter file you have created or the default one in <catkin_ws>/src/jupiter_node/params/jupiter_single.yaml
	especially the serial number.
4. 	roslaunch jupiter_node Jupi.launch
5. 	if the Sensor is running now all topics should be published
	if the Sensor is not running there is a output from the node that there is no data from Jupiter
