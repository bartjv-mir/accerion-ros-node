/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "Visualization.h"

Visualization::Visualization()
{
    pnh_ = new ros::NodeHandle("~");
    nodeNs_ = ros::this_node::getNamespace();
    ROS_INFO("nodeNs_: %s", nodeNs_.c_str());

    pubOutputVelocity_       = nh_.advertise <geometry_msgs::TwistStamped> ("velocity", 1);
    pubOutputLinVelocity_    = nh_.advertise <std_msgs::Float32> ("lin_velocity", 1);
    pubOutputAngVelocity_    = nh_.advertise <std_msgs::Float32> ("ang_velocity", 1);
    subsUncorrectedOdometry_ = nh_.subscribe( "rel_odom", 1, &Visualization::callbackUncorrectedOdometry, this);
}

void Visualization::callbackUncorrectedOdometry(const nav_msgs::Odometry::ConstPtr& odomMsg)
{
    outputVelocityMsg_.header.stamp    = odomMsg->header.stamp;
    outputVelocityMsg_.header.frame_id = odomMsg->child_frame_id;
    outputVelocityMsg_.twist           = odomMsg->twist.twist;

    pubOutputVelocity_.publish(outputVelocityMsg_);

    std_msgs::Float32 xVelMsg;   
    xVelMsg.data = odomMsg->twist.twist.linear.x;
    pubOutputLinVelocity_.publish(xVelMsg);

    std_msgs::Float32 angVelMsg;
    angVelMsg.data = odomMsg->twist.twist.angular.z;
    pubOutputAngVelocity_.publish(angVelMsg);
}