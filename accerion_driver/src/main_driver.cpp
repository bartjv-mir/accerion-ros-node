/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionDriverROS.h"

int main(int argc, char *argv[])
{	
	ros::init(argc, argv, "accerion_driver");
	AccerionDriverROS accDriver;
	
	ros::Rate rate(150);

	while(ros::ok())
	{
		accDriver.run();
		
		ros::spinOnce();

		rate.sleep();
	}

	accDriver.shutdown();
	return 0;
}

