/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef VISUALIZATION_H
#define VISUALIZATION_H

#include <ros/ros.h>

#include <std_msgs/Float32.h>

#include <geometry_msgs/TwistStamped.h>

#include <nav_msgs/Odometry.h>

class Visualization
{
public:
    Visualization();

private:

    void callbackUncorrectedOdometry(const nav_msgs::Odometry::ConstPtr& odomMsg);

    ros::NodeHandle nh_; 
    ros::NodeHandle* pnh_;
    std::string nodeNs_;

    geometry_msgs::TwistStamped outputVelocityMsg_;
    ros::Publisher              pubOutputVelocity_;
    ros::Publisher              pubOutputLinVelocity_;
    ros::Publisher              pubOutputAngVelocity_;

    ros::Subscriber subsUncorrectedOdometry_;
};

#endif // VISUALIZATION_H
