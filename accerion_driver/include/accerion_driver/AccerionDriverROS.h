/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef ACCERIONDRIVERROS_H
#define ACCERIONDRIVERROS_H

#include <bitset>
#include <fstream>
#include <ros/ros.h>

#include <std_msgs/Bool.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PolygonStamped.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/MarkerArray.h>

#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf2_ros/static_transform_broadcaster.h>

#include <accerion_driver_msgs/SensorDiagnostics.h>
#include <accerion_driver_msgs/driftCorrectionDetails.h>

#include <accerion_driver_msgs/SetRecoveryMode.h>
#include <accerion_driver_msgs/Cluster.h>
#include <accerion_driver_msgs/ModeCommand.h>
#include <accerion_driver_msgs/ModeClusterCommand.h>
#include <accerion_driver_msgs/RequestCommand.h>
#include <accerion_driver_msgs/ParentChildFrames.h>
#include <accerion_driver_msgs/Pose.h>
#include <std_srvs/SetBool.h>

#include "UDPManager.h"
#include "utils.h"

class AccerionDriverROS
{
public:
    AccerionDriverROS();

    int run();
    int shutdown();

    uint32_t jupiterSerial_;
    
    struct Pose
    {
        double x;
        double y;
        double th;
    };

    std::string jupiIp_;

private:
    void callbackResetPose(const geometry_msgs::PoseStamped::ConstPtr& resetPose);

    void callbackExternalReferencePose(const nav_msgs::Odometry::ConstPtr& ext_ref_msg);

    bool callbackServiceRecoveryMode(accerion_driver_msgs::SetRecoveryMode::Request  &req,
                                           accerion_driver_msgs::SetRecoveryMode::Response &res);
    bool callbackServiceDeleteCluster(accerion_driver_msgs::Cluster::Request  &req,
                                      accerion_driver_msgs::Cluster::Response &res);
    bool callbackServiceDeleteAllClusters(std_srvs::SetBool::Request  &req,
                                          std_srvs::SetBool::Response &res);
    bool callbackServiceMode(accerion_driver_msgs::ModeCommand::Request  &req,
                             accerion_driver_msgs::ModeCommand::Response &res);
    bool callbackServiceModeWithCluster(accerion_driver_msgs::ModeClusterCommand::Request  &req,
                                        accerion_driver_msgs::ModeClusterCommand::Response &res);
    bool callbackServiceRequest(accerion_driver_msgs::RequestCommand::Request  &req,
                                accerion_driver_msgs::RequestCommand::Response &res);
    bool callbackServiceResetPoseByTfLookup(accerion_driver_msgs::ParentChildFrames::Request  &req,
                                            accerion_driver_msgs::ParentChildFrames::Response &res);
    bool callbackServiceSetPose(accerion_driver_msgs::Pose::Request  &req,
                                accerion_driver_msgs::Pose::Response &res);

    double getThetaFromQuat(const geometry_msgs::Quaternion quatIn);
    
    bool debugModeStreaming_;                            

    /*UDP comm related*/
    UDPManager* udpManager_;
    
    ros::NodeHandle nh_; 
    ros::NodeHandle* pnh_;
    std::string nodeNs_;

    ros::Publisher  pubAbsOdom_, pubRelOdom_, pubAbsCorrections_;
    ros::Publisher  pubDriftDetails_;
    ros::Publisher  pubDiagnostics_;
    ros::Publisher  pubMapAsPC2_;
    ros::Publisher  pubLineFollower_;
    ros::Subscriber subsResetPose_;
    ros::Subscriber subsExternalOdometry_;

    ros::ServiceServer srvServerMode_;
    ros::ServiceServer srvServerModeWithCluster_;
    ros::ServiceServer srvServerRequest_;

    ros::ServiceServer serviceServerRecoveryMode_;
    ros::ServiceServer serviceServerDeleteCluster_;
    ros::ServiceServer serviceServerDeleteAllClusters_;
    ros::ServiceServer serviceServerResetPoseByTfLookup_;      
    ros::ServiceServer serviceServerSetPose_;

    tf::TransformBroadcaster                 tfBroadcaster_;
    tf2_ros::StaticTransformBroadcaster      staticTfBroadcaster_; 
       
    geometry_msgs::TransformStamped  relOdomTrans_, absPoseTrans_;

    std::string   absRefFrame_, relRefFrame_, absBaseFrame_, relBaseFrame_, relSensorFrame_, absSensorFrame_;
    std::string   extRefPoseTopic_;

    bool relOdomOn_, relTransOn_;
    bool absOdomOn_, absTransOn_; 
    bool driftDetailsOn_; 
    bool relInAbsTransOn_;

    int           correctionCounter_;
    bool          sensorConnected_; 
    unsigned long infoCount_, noInfoCount_;

    tf::TransformListener tfListener_;   

    sensor_msgs::PointCloud2            mapPointCloud_;
    sensor_msgs::PointCloud2Modifier*   pc2Modifier_;

    Pose sensorMountPose_;

    double rawTimestampAbs_, rawTimestampRel_;
};

#endif // ACCERIONDRIVERROS_H
